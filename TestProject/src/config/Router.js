import React from "react";
import { Text, Animated, View ,Platform} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Header } from "react-navigation";
import MainListVC from "../scene/MainListVC"
import ItemDetailVC from "../scene/ItemDetailVC"

export const AppNavigator = createStackNavigator(
    {
        MainListVC: { screen: MainListVC },
        ItemDetailVC: { screen: ItemDetailVC },
    },
    {
        defaultNavigationOptions: {
            headerTintColor: "#fff",
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerStyle: {
                backgroundColor: 'white',
            },
            headerBackTitle: null,
        },
    }
);

export default createAppContainer(AppNavigator);