import React, { Component } from "react";
import { BackHandler, Platform, View } from "react-native";

export default class BaseScreenComponent extends Component {
  componentDidMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
  }

  onBackPress = () => {
    if (this.props.navigation.index == 0) {
      return false;
    } else {
      this.props.navigation.goBack();
      return true;
    }
  };

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
  }
}
