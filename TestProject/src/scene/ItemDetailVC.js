import React from "react";
import { Text, Dimensions, FlatList, View, Image, TouchableOpacity } from 'react-native'
import BaseScreenComponent from '../component/BaseScreenComponent'
const { width, height } = Dimensions.get('window')

class ItemDetailVC extends BaseScreenComponent {

    static navigationOptions = {
        title: 'รายละเอียด',
        headerStyle: {
            backgroundColor: '#fff',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {

        const data = this.props.navigation.getParam('data', '{}')

        return <View style={{ flex: 1, flexDirection: 'column' }}>
            <View>{data.score > 0 && data.score < 30 ?
                <Image source={{ uri: data.imageUrl }}
                    resizeMode={'cover'}
                    style={{
                        width: width,
                        height: height * 0.33,
                    }} />
                : null}
            </View>
            <View>{data.score >= 30 && data.score < 60 ?
                <View style={{ flexDirection: 'column' }}>
                    <Image source={{ uri: data.imageUrl }}
                        resizeMode={'cover'}
                        style={{
                            width: width,
                            height: height * 0.33,
                        }} />
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`ชื่อ ${data.name}`}</Text>
                </View>
                : null}
            </View>
            <View>{data.score >= 60 && data.score < 80 ?
                <View style={{ flexDirection: 'column' }}>
                    <Image source={{ uri: data.imageUrl }}
                        resizeMode={'cover'}
                        style={{
                            width: width,
                            height: height * 0.33,
                        }} />
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`ชื่อ ${data.name}`}</Text>
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`นามสกุล ${data.lastName}`}</Text>
                </View>
                : null}
            </View>
            <View>{data.score >= 80 ?
                <View style={{ flexDirection: 'column' }}>
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`ชื่อ ${data.name}`}</Text>
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`นามสกุล ${data.lastName}`}</Text>
                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 8 }}>{`คะแนน ${data.score}`}</Text>
                </View>
                : null}
            </View>
        </View>
    }
}


export default ItemDetailVC