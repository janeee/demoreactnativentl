import React from "react";
import { Text, Dimensions, FlatList, View, Image, TouchableOpacity } from 'react-native'
import BaseScreenComponent from '../component/BaseScreenComponent'
const { width, height } = Dimensions.get('window')

class NewsVC extends BaseScreenComponent {

    static navigationOptions = {
        title: 'หน้าแรก',
        headerStyle: {
            backgroundColor: '#fff',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        let dataMock = [
            {
                name: "test name 1",
                lastName: "test lastname 1",
                score: 20,
                imageUrl: "https://media.wired.com/photos/5b8999943667562d3024c321/master/w_2560%2Cc_limit/trash2-01.jpg"
            },
            {
                name: "test name 2",
                lastName: "test lastname 2",
                score: 25,
                imageUrl: "https://kinsta.com/wp-content/uploads/2017/04/change-wordpress-url-1-1024x512.png"
            },
            {
                name: "test name 3",
                lastName: "test lastname 3",
                score: 30,
                imageUrl: "https://mllj2j8xvfl0.i.optimole.com/Lsv2lkg-oXPY0cKC/w:auto/h:auto/q:98/https://s15165.pcdn.co/wp-content/uploads/2018/06/what-is-a-website-url.jpg"
            },
            {
                name: "test name 4",
                lastName: "test lastname 4",
                score: 22,
                imageUrl: "https://lh3.googleusercontent.com/nGRNKT0qmkYYDlFJ_fzevewjfVgSoUHtr7av9GT4tLw5u32ZyZWXJ3xdZz6ElrOAB1I"
            },
            {
                name: "test name 5",
                lastName: "test lastname 5",
                score: 51,
                imageUrl: "https://www.item-update.com/wp-content/uploads/2020/05/url-facebook-page.jpg"
            },
            {
                name: "test name 6",
                lastName: "test lastname 6",
                score: 70,
                imageUrl: "https://media.wired.com/photos/5b8999943667562d3024c321/master/w_2560%2Cc_limit/trash2-01.jpg"
            },
            {
                name: "test name 7",
                lastName: "test lastname 7",
                score: 80,
                imageUrl: "https://kinsta.com/wp-content/uploads/2017/04/change-wordpress-url-1-1024x512.png"
            },
            {
                name: "test name 8",
                lastName: "test lastname 8",
                score: 9,
                imageUrl: "https://mllj2j8xvfl0.i.optimole.com/Lsv2lkg-oXPY0cKC/w:auto/h:auto/q:98/https://s15165.pcdn.co/wp-content/uploads/2018/06/what-is-a-website-url.jpg"
            },
            {
                name: "test name 9",
                lastName: "test lastname 9",
                score: 20,
                imageUrl: "https://lh3.googleusercontent.com/nGRNKT0qmkYYDlFJ_fzevewjfVgSoUHtr7av9GT4tLw5u32ZyZWXJ3xdZz6ElrOAB1I"
            },
            {
                name: "test name 10",
                lastName: "test lastname 10",
                score: 70,
                imageUrl: "https://www.item-update.com/wp-content/uploads/2020/05/url-facebook-page.jpg"
            },
            {
                name: "test name 11",
                lastName: "test lastname 1",
                score: 65,
                imageUrl: "https://media.wired.com/photos/5b8999943667562d3024c321/master/w_2560%2Cc_limit/trash2-01.jpg"
            },
            {
                name: "test name 12",
                lastName: "test lastname 2",
                score: 35,
                imageUrl: "https://kinsta.com/wp-content/uploads/2017/04/change-wordpress-url-1-1024x512.png"
            },
            {
                name: "test name 13",
                lastName: "test lastname 3",
                score: 78,
                imageUrl: "https://mllj2j8xvfl0.i.optimole.com/Lsv2lkg-oXPY0cKC/w:auto/h:auto/q:98/https://s15165.pcdn.co/wp-content/uploads/2018/06/what-is-a-website-url.jpg"
            },
            {
                name: "test name 14",
                lastName: "test lastname 4",
                score: 83,
                imageUrl: "https://lh3.googleusercontent.com/nGRNKT0qmkYYDlFJ_fzevewjfVgSoUHtr7av9GT4tLw5u32ZyZWXJ3xdZz6ElrOAB1I"
            },
            {
                name: "test name 15",
                lastName: "test lastname 5",
                score: 35,
                imageUrl: "https://www.item-update.com/wp-content/uploads/2020/05/url-facebook-page.jpg"
            },
            {
                name: "test name 16",
                lastName: "test lastname 6",
                score: 72,
                imageUrl: "https://media.wired.com/photos/5b8999943667562d3024c321/master/w_2560%2Cc_limit/trash2-01.jpg"
            },
            {
                name: "test name 17",
                lastName: "test lastname 7",
                score: 10,
                imageUrl: "https://kinsta.com/wp-content/uploads/2017/04/change-wordpress-url-1-1024x512.png"
            },
            {
                name: "test name 18",
                lastName: "test lastname 8",
                score: 78,
                imageUrl: "https://mllj2j8xvfl0.i.optimole.com/Lsv2lkg-oXPY0cKC/w:auto/h:auto/q:98/https://s15165.pcdn.co/wp-content/uploads/2018/06/what-is-a-website-url.jpg"
            },
            {
                name: "test name 19",
                lastName: "test lastname 9",
                score: 67,
                imageUrl: "https://lh3.googleusercontent.com/nGRNKT0qmkYYDlFJ_fzevewjfVgSoUHtr7av9GT4tLw5u32ZyZWXJ3xdZz6ElrOAB1I"
            },
            {
                name: "test name 20",
                lastName: "test lastname 10",
                score: 88,
                imageUrl: "https://www.item-update.com/wp-content/uploads/2020/05/url-facebook-page.jpg"
            },
        ]

        return <FlatList style={{ backgroundColor: 'white' }}
            data={dataMock}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
                <ListItem
                    onPress={this.gotoItemDetail(item)}
                    name={item.name}
                    lastName={item.lastName}
                    score={item.score}
                    imageUrl={item.imageUrl}
                />
            )}
        />
    }

    gotoItemDetail = (item) => () => {
        this.props.navigation.navigate("ItemDetailVC",{
            data:item,
        })
    }
}

const ListItem = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                borderRadius: 8,
                backgroundColor: 'white',
                shadowColor: "#000",
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 2,
                elevation: 2,
                padding: 8,
                marginBottom: 8,
                margin: 8
            }}>
            <View style={{ flexDirection: 'row' }}>
                <Image source={{ uri: props.imageUrl }}
                    resizeMode={'cover'}
                    style={{
                        alignSelf: 'flex-start',
                        width: width / 2.5,
                        height: height * 0.33,
                    }} />
                <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', marginLeft: 8 }}>
                    <Text style={{ fontSize: 20 }}>{`ชื่อ ${props.name}`}</Text>
                    <Text style={{ fontSize: 20 }}>{`นามสกุล ${props.lastName}`}</Text>
                    <Text style={{ fontSize: 20 }}>{`คะแนน ${props.score}`}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}


export default NewsVC