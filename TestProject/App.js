
import React, { Component } from "react";
import RouterComponent from "./src/config/Router";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

class App extends Component {
  render() {
      return <RouterComponent />
  }
}

export default App;